variable "AWS_ACCESS_KEY" {}

variable "AWS_SECRET_KEY" {}

variable "AWS_REGION" {
  description = "AWS Region"
  type        = string
  default     = "ap-southeast-1"
}

variable "service" {
  description       = "variable for service"
  type              = object({
    environment     = string
    name            = string
  })
  default           = {
    environment     = "development"
    name            = "rnd"
  }
}

variable "network" {
  description       = "variable for creating network"
  type              = object({
    cidr            = string
    azs             = list(string)
    private         = list(string)
    public          = list(string)
  })
  default           = {
    cidr            = "10.0.0.0/21"
    azs             = ["ap-southeast-1a", "ap-southeast-1b", "ap-southeast-1c"]
    public          = ["10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24"]
    private         = ["10.0.3.0/24", "10.0.4.0/24", "10.0.5.0/24"]
  }
}
