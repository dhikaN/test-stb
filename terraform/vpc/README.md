
## Requirements ##

| Name | Version |
| terraform | >= 0.12.0 |

## Default Configuration Network
You can find all variable in the `variable.tf`

|  Name | Description  |
| cidr  | 10.0.0.0/21 |
| AZ  | "ap-southeast-1a", "ap-southeast-1b", "ap-southeast-1c" |
| Public Subnet  | "10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24" |
| Private Subnet  | "10.0.3.0/24", "10.0.4.0/24", "10.0.5.0/24" |

## How to use
Create an account using the IAM and enable programetic access Then you will get the secret key access key. create a file with the name "terraform.tfvars" then fill it with
AWS_ACCESS_KEY = "YOUR_ACCESS_KEY"
AWS_SECRET_KEY = "YOUR_SECRET_KEY"
