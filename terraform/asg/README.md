# Terraform ASG #
ASG that placed in the private subnet. you need to create network in the folder `vpc` Then you will get the VPC_ID. Safe this VPC and then put this on the `variable.tf`.


| Name | Version |
| terraform | >= 0.12.0 |

## How to use
create an account using the IAM. Then you will get the secret key access key. create a file with the name `terraform.tfvars` then fill it with
AWS_ACCESS_KEY = "YOUR_ACCESS_KEY"
AWS_SECRET_KEY = "YOUR_SECRET_KEY"
